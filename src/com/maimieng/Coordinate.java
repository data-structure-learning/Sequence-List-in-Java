package com.maimieng;

/**
 * Created by kingcos on 23/09/2016.
 */
public class Coordinate {
    private int x;
    private int y;

    public Coordinate(int x, int y) {
        this.x = x;
        this.y = y;
    }

    @Override
    public String toString() {
        return "(" + x + ", " + y + ")";
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }

        if (obj instanceof Coordinate) {
            Coordinate t = (Coordinate)obj;
            return t.x == this.x && t.y == this.y;
        }

        return false;
    }
}
