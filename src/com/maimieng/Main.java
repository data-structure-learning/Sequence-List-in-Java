package com.maimieng;

public class Main {

    public static void main(String[] args) {
        System.out.println("testIntSequenceList");
        testIntSequenceList();

        System.out.println("testObjectSequenceList");
        testObjectSequenceList();
    }

    static void testIntSequenceList() {
        SequenceList<Integer> sqList = new SequenceList<>(5);

        sqList.insertElem(1, 0);
        sqList.insertElem(2, 1);
        sqList.insertElem(3, 2);
        sqList.insertElem(4, 3);
        sqList.insertElem(10, 1);

        sqList.listTraverse();

        sqList.deleteElemAt(0);

        sqList.listTraverse();

        sqList.deleteElemAt(0);

        sqList.listTraverse();

        int e = sqList.getElemAt(0);
        System.out.println("e: " + e);

        e = sqList.getPreElem(3);
        System.out.println("e Pre: " + e);

        e = sqList.getNextElem(3);
        System.out.println("e Next: " + e);

        sqList.clearList();

        sqList.listTraverse();

    }

    static void testObjectSequenceList() {
        SequenceList<Coordinate> sqList = new SequenceList<>(5);

        sqList.insertElem(new Coordinate(1, 1), 0);
        sqList.insertElem(new Coordinate(2, 2), 1);
        sqList.insertElem(new Coordinate(3, 3), 2);
        sqList.insertElem(new Coordinate(4, 4), 3);

        sqList.insertElem(new Coordinate(10, 10), 1);

        sqList.listTraverse();

        sqList.deleteElemAt(0);

        sqList.listTraverse();

        sqList.deleteElemAt(0);

        sqList.listTraverse();

        Coordinate e = sqList.getElemAt(0);
        System.out.println("e: " + e);

        e = sqList.getPreElem(new Coordinate(3, 3));
        System.out.println("e Pre: " + e);

        e = sqList.getNextElem(new Coordinate(3, 3));
        System.out.println("e Next: " + e);

        sqList.clearList();

        sqList.listTraverse();
    }
}
