package com.maimieng;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by kingcos on 23/09/2016.
 */
public class SequenceList<T> {
    private List<T> m_pList;
    private int m_iSize;
    private int m_iLen;

    public SequenceList(int m_iSize) {
        this.m_iSize = m_iSize;
        m_pList = new ArrayList<>();
        clearList();
    }

    public void clearList() {
        m_iLen = 0;
    }

    public boolean isEmpty() {
        return m_iLen == 0;
    }

    public boolean isFull() {
        return m_iLen == m_iSize;
    }

    public T getElemAt(int index) {
        if (index < 0 || index >= m_iLen) {
            return null;
        }

        return m_pList.get(index);
    }

    public int locateElem(T elem) {
        for (int i = 0; i < m_iLen; i ++) {
            if (m_pList.get(i).equals(elem)) {
                return i;
            }
        }

        return -1;
    }

    public T getNextElem(T curElem) {
        int t = locateElem(curElem);

        if (t == - 1 || t == 0) {
            return null;
        }

        return m_pList.get(t + 1);
    }

    public T getPreElem(T curElem) {
        int t = locateElem(curElem);

        if (t == m_iLen - 1) {
            return null;
        }

        return m_pList.get(t - 1);
    }

    public boolean insertElem(T elem, int index) {
        if (index < 0 || index > m_iLen || isFull()) {
            return false;
        }

        if (index < m_iLen) {
            m_pList.add(m_pList.get(m_iLen - 1));
        }
        for (int j = m_iLen - 2; j >= index; j --) {
            m_pList.set(j + 1, m_pList.get(j));
        }

        if (m_pList.size() <= index) {
            m_pList.add(elem);
        } else {
            m_pList.set(index, elem);
        }
        m_iLen += 1;
        return true;
    }

    public boolean deleteElemAt(int index) {
        if (index < 0 || index >= m_iLen || isEmpty()) {
            return false;
        }


        for (int j = index; j < m_iLen - 1; j++) {
            m_pList.set(j, m_pList.get(j + 1));
        }

        m_iLen -= 1;
        return true;
    }

    public void listTraverse() {
        for (int i = 0; i < m_iLen; i ++) {
            System.out.print(m_pList.get(i) + " ");
        }

        System.out.println();
    }

}
